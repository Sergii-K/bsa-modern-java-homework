package com.binary_studio.academy_coin;

import java.util.stream.Stream;

public final class AcademyCoin {

    private AcademyCoin() {
    }

    public static int maxProfit(Stream<Integer> prices) {

        Integer[] pricesArray = prices.toArray(Integer[]::new);

        if (!verifyPositivePrices(pricesArray)) {
            System.out.println("Incorrect input. Price cannot be negative...Well, it can, but it's pointless");

            return 0;
        } else {

            return calculateMaxProfit(pricesArray);
        }
    }

    private static int calculateMaxProfit(Integer[] prices) {
        int result = 0;
        int max = 0;
        int min;

        try {
            min = prices[0];
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("Incorrect input. Input is NULL or empty stream!");
            return 0;
        }

        for (int i = 0; i < prices.length - 1; i++) {

            if (prices[i] < prices[i + 1]) {
                if (prices[i] < min) {
                    min = prices[i];
                }
                if (prices[i + 1] > max) {
                    max = prices[i + 1];
                }
            }

            if (prices[i] > prices[i + 1]) {
                result += prices[i] - min;
                min = prices[i + 1];
            }

            if (i == prices.length - 2) {
                if (prices[prices.length - 1] == max) {
                    result += max - min;
                }
            }
        }

        return result;
    }

    private static boolean verifyPositivePrices(Integer[] prices) {
        for (Integer price : prices) {
            if (price < 0) {
                System.out.println("Price cannot be negative");
                return false;
            }
        }

        return true;
    }

}
