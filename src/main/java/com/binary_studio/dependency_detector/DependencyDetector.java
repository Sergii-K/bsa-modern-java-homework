package com.binary_studio.dependency_detector;

public final class DependencyDetector {

    private DependencyDetector() {
    }

    public static boolean canBuild(DependencyList dependencyList) {
        if (dependencyList.getDependencies().isEmpty()) {

            return true;
        }

        for (String[] libraryArray : dependencyList.getDependencies())
            if (libraryArray.length > 2) {
                System.out.println("Incorrect input. More than 2 libraries in one section.");
                return false;
            }

        String listFirstDependency = dependencyList.getDependencies().get(0)[0];
        String previousDependency = "";

        for (int i = 0; i < dependencyList.getDependencies().size(); i++) {
            previousDependency = dependencyList.getDependencies().get(i)[0];
            if (dependencyList.getDependencies().get(i)[1].equals(previousDependency)
                    || dependencyList.getDependencies().get(i)[1].equals(listFirstDependency)) {

                return false;
            }
        }

        return true;
    }

}
